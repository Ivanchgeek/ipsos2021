from telegram import Update, ReplyKeyboardMarkup
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
import json

moderator_chat_id: int
channel_id: str
token: str

help_message = "Здравствуйте!\n"
help_message += "С помощью этого бота вы можете рассказать свои истории о маленьких новогодних чудесах, которые с вами происходили.\n"
help_message += "Чтобы отправить свою историю нажмите на кнопку /send и следуйте дальнейшим инструкциям.\n"
help_message += "Чтобы еще раз увидеть это сообщение, воспользуйтесь кнопкой /help.\n"
help_message += "Чтобы увидеть истории других пользователей, перейдите по ссылке https://t.me/ipsos_2021\n"
help_message += "Счастливого Нового года!!!"

reply_markup = ReplyKeyboardMarkup([['/help', '/send']], one_time_keyboard=False, per_user=True)

sendCondidates = []

def start(update: Update, context: CallbackContext) -> None:
    help_command(update, context)


def help_command(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(help_message, reply_markup=reply_markup)

def send_command(update: Update, context: CallbackContext) -> None:
    sendCondidates.append(update.message.from_user.id)
    update.message.reply_text('Я вас слушаю! Отправьте свою историю одним сообщением.')

def service_command(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('chat_id: {}'.format(update.message.from_user.id))

def flood_handler(update: Update, context: CallbackContext) -> None:
    id = update.message.from_user.id
    if id in sendCondidates:
        sendCondidates.remove(id)
        send_to_moderator('История от пользователя {0} {1}:\n\n'.format(update.message.from_user.first_name, update.message.from_user.last_name)+update.message.text, context)
        update.message.reply_text('Спасибо, ваша история отправлена на модерацию.\nЧтобы увидеть истории других пользователей, перейдите по ссылке https://t.me/ipsos_2021', reply_markup=reply_markup)
    else:
        if (id == moderator_chat_id) & (update.message.reply_to_message != None):
            send_to_channel(update.message.reply_to_message.text, context)
        else:
            update.message.reply_text('Я вас не понимаю, воспользуйтесь кнопкой /help, чтобы прочитать инструкцию.', reply_markup=reply_markup)

def send_to_moderator(story: str, context: CallbackContext):
    context.bot.sendMessage(moderator_chat_id, story)

def send_to_channel(story: str, contex: CallbackContext):
    contex.bot.sendMessage(channel_id, story)

def main():
    global moderator_chat_id
    global token
    global channel_id
    with open('./config.json') as json_file:
        data = json.load(json_file)
        token = data['bot_token']
        moderator_chat_id = data['moderator_chat_id']
        channel_id = data['channel_id']

    updater = Updater(token, use_context=True)
    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("send", send_command))
    dispatcher.add_handler(CommandHandler("service", service_command))
    dispatcher.add_handler(MessageHandler(Filters.text | Filters.command, flood_handler))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()