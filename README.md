# Ipsos 2021 postcard

## Запуск сайта

* распоковать zip архив с билд версиией сайта (лежит в корне репозитория)
* сайт можно сервить любым удобным вам файл сервером (например nginx, [инструкция для него](https://medium.com/@timmykko/deploying-create-react-app-with-nginx-and-ubuntu-e6fe83c5e9e7))


## Конфигурация бота

Скрипт бота находится в папке back, в этом же каталоге создать файл config.json вида

```
{
    "bot_token": "token",
    "moderator_chat_id": int_id,
    "channel_id": "str_id"
}
```

Значения полей bot_token и channel_id лежат на почте, получить значение moderator_chat_id можно через бота
отправив комманду /service с аккаунта модератора.

## Запуск бота
Выполнить команду
```
python ./bot.py
```
находясь в директории back