import React, { useState, useEffect, useRef } from 'react';
import '../styles/App.sass'

function Snow() {

    const canvas = useRef(null);
    const particlesCount = 1000;
    var [particles, setParticles] = useState([]);
    var width, height;
    var [active, setActive] = useState(false)

    useEffect(() => {
        // eslint-disable-next-line
        width = canvas.current.clientWidth;
        canvas.current.width = width;
        // eslint-disable-next-line
        height = canvas.current.clientHeight;
        canvas.current.height = height;
        var particlesBuf = particles
        for (let i = 0; i < particlesCount; i++) {
            particlesBuf.push({
                x: width * Math.random(),
                y: height * Math.random(),
                r: 4 * Math.random()
            })   
        }
        setParticles(particlesBuf)
        if (!active) {
            drawSnow()
            setActive(true)
        }
    });

    function drawSnow() {
        let ctx = canvas.current.getContext('2d');
        ctx.clearRect(0, 0, width, height);
        ctx.fillStyle = 'white';
        var particlesBuf = particles
        for (let i = 0; i < particlesCount; i++) {
            ctx.beginPath();
            ctx.arc(particles[i].x, particles[i].y, particles[i].r, 0, 2 * Math.PI, false);
            particlesBuf[i].y += 1;
            if (particlesBuf[i].y > height) {
                particlesBuf[i].y = 0;
                particlesBuf[i].x = width * Math.random();
            }
            setParticles(particlesBuf)
            ctx.fill();
        }
        requestAnimationFrame(drawSnow);
    }

    return (
        <canvas className='snow' ref={canvas}></canvas>
    );
}

export default Snow;