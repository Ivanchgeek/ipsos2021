import React, { useState, useEffect, useRef } from 'react';
import '../styles/App.sass'
import background_1 from "../stuff/Ipad/fon_1.png"
import background_2 from "../stuff/Ipad/fon_2.png"
import background_3 from "../stuff/Ipad/fon_3.png"
const backgrounds = [background_1, background_2, background_3]

function Snow(props) {

    var [backgroundIndex, setBackgroundIndex] = useState(0)
    var [backgroundBuff, setBackgroundBuff] = useState(0)
    var main = useRef(null)

    useEffect(() => {
        setBackgroundBuff(props.frame)
    }, [props.frame])

    if (backgroundIndex !== backgroundBuff) {
        main.current.animate([
            {opacity: 1},
            {opacity: 0}
        ], {
            duration: 500,
            fill: 'forwards'
        })
        setTimeout(() => {
            setBackgroundIndex(backgroundBuff)
            setTimeout(() => {
                main.current.style.opacity = '1'
            }, 1)
        }, 500)
    }

    return (
        <>
            <img className='background' src={backgrounds[backgroundBuff]} alt='background fade'></img>
            <img className='background' src={backgrounds[backgroundIndex]} alt='background' ref={main}></img>
        </>
    );
}

export default Snow;