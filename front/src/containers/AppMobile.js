import React, { useState, useEffect, useRef } from 'react';
import { connect } from "react-redux"
import '../styles/App.sass'
import Snow from './Snow'
import Background from './Background'
import StartFrame from './StartFrame'
import StoryFrame from './StoryFrame'
import FinishFrame from './FinishFrame'
import logo from '../stuff/logo/logo.png'
import mute from '../stuff/mute.png'
import unmute from '../stuff/unmute.png'
import {StartMusic, PauseMusic} from '../actions/music'

function AppMobile(props) {

    var [urlParsed, setUrlParsed] = useState(false)
    var [orientation, setOrientation] = useState()

    useEffect(() => {
        setOrientation(getOritntation())
    })

    if ('onorientationchange' in window) {
        window.addEventListener("orientationchange", function () {
            window.location.reload()
            setOrientation(getOritntation())
        }, false);
    }

    const getOritntation = () => {
        return window.orientation === 0 ? "portrait" : "landscape"
    }

    useEffect(() => {
        let imgs = document.getElementsByTagName('img')
        for (var i = 0; i < imgs.length; i++) {
            imgs[i].ondragstart = () => { return false }
        }
    })

    useEffect(() => {
        if (!urlParsed) {
            setUrlParsed(true)
            let parse = parseInt(new URLSearchParams(window.location.search).get('story'))
            if (parse !== undefined && !isNaN(parse)) {
                props.dispatch({type: 'SetStoryPreset', value: parse})
                props.dispatch({type: 'SetFrame', frame: 1})
            }
        }
    })

    const FrameSelecter = () => {
        switch (props.frame) {
            case 0:
                return (<StartFrame></StartFrame>)
            case 1:
                return (<StoryFrame></StoryFrame>)
            case 2:
                return (<FinishFrame></FinishFrame>)
            default:
                return (<h1>EMPTY FRAME</h1>)
        }
    }

    const muteSwitch = () => {
        if (props.muted) {
            StartMusic()
            props.dispatch({type: 'SetMusicState', value: false})
        } else {
            PauseMusic()
            props.dispatch({type: 'SetMusicState', value: true})
        }
    }

    var view = (
        <div>
            <div className='tablet mobile'>
                {/* <img className='face' src={tablet} alt='tablet'></img> */}
                <Snow></Snow>
                <Background frame={props.frame}></Background>
                <FrameSelecter></FrameSelecter>
                <img className='logo' src={logo} alt='logo'></img>
                <img className='myte' src={props.muted ? unmute : mute} alt='mute' onClick={muteSwitch}></img>
            </div>
        </div>
    )
    if (orientation === 'portrait') {
        view = (
            <div>
            <div className='tablet mobile'>
                {/* <img className='face' src={tablet} alt='tablet'></img> */}
                <Snow></Snow>
                <h1 className='rotate'>Поверните телефон, чтобы прикоснуться к чуду.</h1>
            </div>
        </div>
        )
    }

    return view
}

const mapStateToProps = (state) => ({
    frame: state.frames.frame,
    muted: state.music.muted
});
export default connect(mapStateToProps)(AppMobile)