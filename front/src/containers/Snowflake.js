import React, { useState, useEffect, useRef } from 'react';
import '../styles/App.sass'
import snowflakeSrc from '../stuff/Ipad/snow_TEXT.svg'
import snowflakeBlueSrc from '../stuff/Ipad/snow_TEXT_blue.svg'

function Snowflake(props) {
    return (
        <div className='snowflake'><img src={props.blue ? snowflakeBlueSrc : snowflakeSrc} alt='snowflake'></img></div>
    )
}

export default Snowflake