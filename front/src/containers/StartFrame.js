import React, { useState, useEffect, useRef } from 'react';
import {connect} from "react-redux"
import '../styles/App.sass'
// import carrotIndex from '../stuff/morkov/morkov_index.svg'
import Snowflake from './Snowflake'

function StartFrame(props) {

    const swipeHelpLimit = 0.2

    var [startY, setStartY] = useState(0)
    var [mousePressed, setMousePressed] = useState(false)
    var swipe = useRef(null)
    var label = useRef(null)
    var carrot = useRef(null)

    const mouseDown = event => {
        setStartY(event.clientY)
        swipeStart()
    }

    const touchStart = event => {
        setStartY(event.touches[0].clientY)
        swipeStart()
    }

    const swipeStart = () => {
        setMousePressed(true)
        swipe.current.style.transition = '0'
        label.current.style.transition = '0'
        // carrot.current.style.transition = '0'
    }

    const mouseMove = event => {
        processMove(
            event.clientY, 
            () => {
                if ((startY - event.clientY) / event.target.clientHeight >= swipeHelpLimit) 
                    mouseUp(event)
            }, 
            () => {
                setStartY(event.clientY)
            }
        )
    }

    const touchMove = event => {
        processMove(
            event.touches[0].clientY,
            () => {},
            () => {
                setStartY(event.touches[0].clientY)
            }
        )
    }

    const processMove = (y, upMonitor, setStart) => {
        if (mousePressed) {
            if ((y - startY) < 0) {
                swipe.current.style.transform = `translate(-50%, ${y - startY}px)`
                label.current.style.transform = `translate(-50%, ${y - startY}px)`
                // carrot.current.style.transform = `translateY(${startY - y}px)`
                upMonitor()
            } else {
                setStart()
            }
        }
    }

    const mouseUp = event => {
        if (mousePressed) {
            setMousePressed(false)
            if ((startY - event.clientY) / event.target.clientHeight >= swipeHelpLimit) {
                helpSwipe()
            } else {
                scratchSwipe()
            }
        }
    }

    const touchEnd = event => {
        if (mousePressed) {
            setMousePressed(false)
            if ((startY - event.changedTouches[0].clientY) / event.target.clientHeight >= swipeHelpLimit) {
                helpSwipe()
            } else {
                scratchSwipe()
            }
        }
    }

    const mouseOut = event => {
        if (mousePressed) {
            setMousePressed(false)
            scratchSwipe()
        }
    }

    const helpSwipe = () => {
        swipe.current.style.transition = '.5s'
        label.current.style.transition = '.5s'
        // carrot.current.style.transition = '.5s'
        setTimeout(() => {
            swipe.current.style.transform = `translate(-50%, -100vh)`
            label.current.style.transform = `translate(-50%, -100vh)`
            // carrot.current.style.transform = `translateY(100vh)`
            setTimeout(() => {
                props.dispatch({type: 'NextFrame'})
            }, 500)
        })
    }

    const scratchSwipe = () => {
        swipe.current.style.transition = '.5s'
        label.current.style.transition = '.5s'
        // carrot.current.style.transition = '.5s'
        setTimeout(() => {
            swipe.current.style.transform = `translate(-50%, 0)`
            label.current.style.transform = `translate(-50%, 0)`
            // carrot.current.style.transform = `translateY(0)`
        })
    }

    return (
        <div className='startFrame' onMouseDown={mouseDown} onMouseMove={mouseMove} onMouseUp={mouseUp} onMouseOut={mouseOut} onTouchStart={touchStart} onTouchMove={touchMove} onTouchEnd={touchEnd}>
            <div className='storiesLabel' ref={label}>
                НОВОГОДНИЕ<br/>
                <Snowflake blue></Snowflake> ИСТОРИИ <Snowflake blue></Snowflake> <br/>
                <div className='by'>by Ipsos</div>
            </div>
            <div className='swipe' ref={swipe}>СМАХНИТЕ ВВЕРХ</div>
            {/* <img className='carrotIndex' src={carrotIndex} alt='carrot index' ref={carrot}></img> */}
        </div>
    );
}

const mapStateToProps = (state) => ({
});
export default connect(mapStateToProps)(StartFrame)