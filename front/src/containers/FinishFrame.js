import React, { useState, useEffect, useRef } from 'react';
import {connect} from "react-redux"
import '../styles/App.sass'
import Snowflake from './Snowflake'
import snowmanSrc from '../stuff/snowman/snowman_full.png'
import Share from './Share'
import refreshSrc from '../stuff/refresh.svg'

function FinishFrame(props) {

    var wish = useRef(null)
    var snowman = useRef(null)
    var share = useRef(null)
    var restartBtn = useRef(null)

    useEffect(() => {
        if (!props.appearAnimated) {
            props.dispatch({type: 'FinishAnimated'})
            wish.current.style.top = '10%'
            snowman.current.style.bottom = '10%'
            share.current.style.transition = '0s'
            restartBtn.current.style.bottom = '9vh'
            setTimeout(() => {
                share.current.style.left = '50%'
                share.current.style.transform = 'translateX(-50%)'
                share.current.style.transition = '.5s'
                setTimeout(() => {
                    share.current.style.top = 'calc(10.7vh + 70%)'
                }, 500)
            }, 1)
        }
    })

    const restart = () => {
        window.location = window.location.origin
    }

    return (
        <div className={`finishFrame ${props.appearAnimated ? 'animated' : ''}`}>
            <div className='wish' ref={wish}>
                ЖЕЛАЕМ БОЛЬШЕ<br/>
                <Snowflake></Snowflake>ХОРОШИХ ИСТОРИЙ<Snowflake></Snowflake> <br/>
                В НОВОМ ГОДУ!
            </div>
            <img src={snowmanSrc} alt='snowman' className='snowman' ref={snowman}></img>
            <img src={refreshSrc} alt='refresh' className='refresh' onClick={restart} ref={restartBtn}></img>
            <Share ref={share} label='поделись в соц сетях -<br/> сделай мир лучше' small></Share>
        </div>
    );
}

const mapStateToProps = (state) => ({
    appearAnimated: state.frames.finishAnimated
});
export default connect(mapStateToProps)(FinishFrame)