import React, { useState, useEffect, useRef } from 'react';
import '../styles/App.sass'
import fbSrc from '../stuff/icons_social/fb.png'
import vkSrc from '../stuff/icons_social/vk.png'
import tgSrc from '../stuff/icons_social/telegt.png'
import waSrc from '../stuff/icons_social/whats.png'

const Share = React.forwardRef((props, ref) => {

    const imageUrl = 'https://i.ibb.co/255tqsH/pic-selected-201213-1757-30.png'

    const vk = () => {
        open(`https://vk.com/share.php?url=${generateSelfUrl()}&title=${generateTitle()}&image=${imageUrl}`)
    }

    const fb = () => {
        console.log(generateSelfUrl())
        open(`https://facebook.com/sharer/sharer.php?u=${generateSelfUrl()}&t=${generateTitle()}`)
    }

    const tg = () => {
        open(`https://t.me/share/url?url=${generateSelfUrl()}`)
    }

    const wa = () => {
        open(`https://api.whatsapp.com/send?text=${generateSelfUrl()}`)
    }

    const open = url => {
        let win = window.open(url, '_blank');
        win.focus();
    }

    const generateSelfUrl = () => {
        return window.location + (props.story ? `?story=${props.story}` : '')
    }

    const generateTitle = () => {
        return props.title ? props.title : 'НОВОГОДНИЕ ИСТОРИИ ОТ IPSOS'
    }

    return (
        <div className='share' ref={ref}>
            <span className={props.small ? 'small' : 'big'} contentEditable='true' dangerouslySetInnerHTML={{ __html: props.label }}></span>
            <img src={vkSrc} className='icon' onClick={vk}></img>
            <img src={fbSrc} className='icon' onClick={fb}></img>
            <img src={tgSrc} className='icon' onClick={tg}></img>
            <img src={waSrc} className='icon' onClick={wa}></img>
        </div>
    );
})

export default Share