const initialState = {
    frame: 0,
    storyAppearAnimated: false,
    storyChangeAnimated: false,
    story: 0,
    storyPreseted: false,
    storyPreset: -1,
    finishAnimated: false
}

export default function log(state = initialState, action) {
    switch (action.type) {
        case 'NextFrame':
            return (
                {
                    ...state,
                    frame: state.frame+1
                }
            )
        case 'SetFrame':
            return (
                {
                    ...state,
                    frame: action.frame
                }
            )
        case 'StoryAppeared': 
            return (
                {
                    ...state,
                    storyAppearAnimated: true
                }
            )
        case 'StoryChanged': 
            return (
                {
                    ...state,
                    storyChangeAnimated: true
                }
            )
        case 'SetStory': 
            return (
                {
                    ...state,
                    story: action.value
                }
            )
        case 'SetStoryPreset':
            return (
                {
                    ...state,
                    storyPreset: action.value
                }
            )
        case 'SetStoryPreseted':
            return (
                {
                    ...state,
                    storyPreseted: action.value
                }
            )
        case 'FinishAnimated':
            return (
                {
                    ...state,
                    finishAnimated: true
                }
            )
        default:
            return state
    }
}
