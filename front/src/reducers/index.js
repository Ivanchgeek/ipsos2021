import {combineReducers} from "redux"
import frames from "./frames"
import music from "./music"

export default combineReducers({
    frames,
    music
})
