const initialState = {
    muted: false
}

export default function log(state = initialState, action) {
    switch (action.type) {
        case 'SetMusicState':
            return (
                {
                    ...state,
                    muted: action.value
                }
            )
        default:
            return state
    }
}
