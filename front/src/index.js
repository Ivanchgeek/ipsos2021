import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/App'
import AppMobile from './containers/AppMobile'
import reportWebVitals from './reportWebVitals'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import {BrowserView, MobileView} from 'react-device-detect'

const store = configureStore();

console.log('--------')
console.log('created by ivanchgeek')
console.log('https://t.me/ivanchgeek')
console.log('https://gitlab.com/Ivanchgeek')
console.log('--------')


ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserView><App/></BrowserView>
      <MobileView><AppMobile/></MobileView>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
