import music1 from '../stuff/music/music1.mp3'
import music2 from '../stuff/music/music2.mp3'
import music3 from '../stuff/music/music3.mp3'

const musicStack = [music1, music2, music3]
var music
var playing = false

export const StartMusic = (track = 0) => {
    if (!playing || track !== 0) {
        playing = true
        music = new Audio()
        if (track === 0) music.play().catch((e) => {})
        music.src = musicStack[track]
        music.load()
        music.autoplay = true
        music.play().catch((e) => {})
        music.onended = () => {
            if (track >= musicStack.length) {
                playing = false
                StartMusic()
            } else {
                StartMusic(track + 1)
            }
        }
    }
}

export const PauseMusic = () => {
    if (music !== undefined) {
        music.pause()
        playing = false
    }
}